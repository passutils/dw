use std::fs;

use rand::Rng;
use rand_core::OsRng;

pub fn gen_new(length: usize, path: Option<String>) -> String {
    let mut sentence = String::new();
    let length = if length == 0 { 8 } else { length };
    for _ in 0..length {
        let mut random = String::new();
        for _ in 0..5 {
            let number = OsRng.gen_range(1..6);
            random.push_str(number.to_string().as_str());
        }
        sentence.push_str(find_word(&random, path.as_ref()).unwrap().as_str().as_ref());
    }
    sentence[1..sentence.len()].to_string()
}

fn find_word(line: &str, path: Option<&String>) -> std::result::Result<String, String> {
    let mut word_list = include_str!("../../assets/wordlist.txt").to_string();
    if path.is_some() {
        word_list = fs::read_to_string(path.unwrap()).unwrap();
    }
    for i in word_list.lines() {
        if i.starts_with(line) {
            let word = i.split("	").collect::<Vec<&str>>();
            return Ok(format!(" {}", word.last().unwrap().to_string()));
        }
    }
    return Err("Word not found".to_string());
}
