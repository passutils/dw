use dw::diceware;
use clap::Parser;

#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
struct Args {
   #[clap(short, long, value_parser)]
   path: Option<String>,

   #[clap(short, long, value_parser, default_value_t = 8)]
   length: usize,
}



fn main() {
    let args = Args::parse();
    println!("{}", diceware::generate::gen_new(args.length, args.path));
}
